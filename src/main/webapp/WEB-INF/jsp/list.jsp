<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>


<style type="text/css">




#task
{
}

h1
{
text-align:center;
}
</style>

<div>
	<h1><s:message code="view.list.addTask"/></h1>
	<portlet:actionURL var="addTaskUrl"><portlet:param name="action" value="addTask"/></portlet:actionURL>
	<div>
		<form:form action="${addTaskUrl}" method="post" commandName="addTask">
		<table cellpadding="4">
			<tr>
				<td>
					<p>
 						<s:message code="view.list.day"/>:
 					</p>
 				</td>
 				<td>
 					<p>
  						<form:input path="day" cssErrorClass="field-error"/>
  					</p>
  				</td>
  				<td>
  					<p>
   						<s:message code="view.list.month"/>:
   					</p>
   				</td>
   				<td>
   					<p>
   						<form:input path="Month" cssErrorClass="field-error"/>			
   					</p>
   				</td>
   				<td>
   					<p>
   						<s:message code="view.list.year"/>: 
   					</p>
   				</td>
   				<td>
   					<p>
   						<form:input path="Year" cssErrorClass="field-error"/>
   					</p>
   				</td>
   			</tr>				
 			<tr>
 				<td>
 					<p>
 				 		<s:message code="view.list.hour"/>: 
 				 	</p>
 				 </td>
 				 <td>
 				 	<p>
	 					<form:input path="Hour" cssErrorClass="field-error"/>
	 				</p>
	 			</td>
	 			<td>
	 				<p>
 						<s:message code="view.list.minutes"/>: 
 					</p>
 				</td>
 				<td>
 					<p>
 						<form:input path="Minutes" cssErrorClass="field-error"/> 	
 					</p>
 				</td>
 				<td>
 					<p>
 						<s:message code="view.list.duration"/>: 
 					</p>
 				</td>
 				<td>
 					<p>
 						<form:input path="Duration" cssErrorClass="field-error"/> 
 					</p>
 				</td>
 			</tr>
			<tr>
				<td>
					<p>
						<s:message code="view.list.title"/>:
					</p>
				</td>
				<td>
					<p>
 						<form:input path="Title" cssErrorClass="field-error"/>
 					</p>
 				</td>
 			</tr>
 		</table>
			<p>
 				<s:message code="view.list.description"/>:
 			</p>
 			<p>
	 			<form:input path="Description" cssErrorClass="field-error" style="width:400px; height:80px"/>
			</p>
			<br></br>
			<p>
				<button type="submit"><s:message code="view.action.create"/></button>
			</p>
 				
		</form:form>
	</div>
</div>

<p> = <s:message code="view.list.title"/> = </p>
<c:forEach items="${map}" var="task">
	
  <portlet:actionURL var="deleteTaskUrl">
    <portlet:param name="action" value="deleteTask"/>
    <portlet:param name="id" value="${task.id}"/>
  </portlet:actionURL>
  <p>${task} | <a href="${deleteTaskUrl}"><s:message code="view.action.delete"/></a>
</p>
</c:forEach>
</jsp:root>
