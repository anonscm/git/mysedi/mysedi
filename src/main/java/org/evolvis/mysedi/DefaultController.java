package org.evolvis.mysedi;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;

@Controller
@RequestMapping(value = "view")
public class DefaultController {
	
	private final Log log = LogFactory.getLog(getClass());
	private String id;
	
	private static final HashMap<String,Complete> listMap= new HashMap<String,Complete>();
	
//	private static final Map<Hour,Task> listMap = Collections.synchronizedMap(new TreeMap<Hour,Task>(new HourComparator()){
//		@Override
//		public Task put(Hour key, Task value) {
//			Task oldValue = this.get(key);
//			if (oldValue != null) {
//				throw new RuntimeException("A Value for the key:" + key + " is always defined: " + oldValue);
//			}
//			else {
//				super.put(key, value);
//				return null;
//			}
//		};		
//	}); //new HashMap<Hora,Tarea>());

    @RequestMapping
    public ModelAndView defaultRenderRequest() {
    	ModelAndView mav = new ModelAndView("list");
    	mav.addObject("addTask", new Complete());
    	
        mav.addObject("map", listMap.values());
        return mav;
        
    }
    
    //metodo que crea la nueva tarea
    @RequestMapping(params = "action=addTask")
    public void addTaskAction(Complete addTask) {
    	
    	addTask.setId(UUID.randomUUID().toString());
//    	int hourInt, minutesInt;
//    	try {
//    		hourInt = Integer.parseInt(addTask.getHour());
//    		minutesInt = Integer.parseInt(addTask.getMinutes());
//    	}
//    	catch (Exception e)
//    	{
//    		log.info("parseInt failed ... do it better (logging)!");
//    		return;
//    	}
    	
//    	Complete complete = new Complete(addTask.getHour(),addTask.getMinutes(),addTask.getDay(),addTask.getMonth(),
//    			addTask.getYear(),addTask.getTitle(),addTask.getDescription(),addTask.getDescription());
    	
    	listMap.put(addTask.getId(), addTask);
    }
    //metodo que borra una tarea
    @RequestMapping(params = "action=deleteTask")
    public void deleteTaskAction(@RequestParam(value = "id") String id) {
    	log.info(listMap.get(id));
//    	try {
//    		horaInt = Integer.parseInt(complete.getHour());
//    		minutosInt = Integer.parseInt(complete.getMinutes());
//    	}
//    	catch (Exception e)
//    	{
//    		log.info("parseInt failed ... do it better (logging)!");
//    		return;
//    	}
//    	
//    	Hour hora = new Hour(horaInt, minutosInt);
//    	
    	listMap.remove(id);
    }
}
