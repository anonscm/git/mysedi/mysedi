package org.evolvis.mysedi;

public class Date
{
    private Integer day;
    private Integer month;
    private Integer year;
    

    public Date()
    {
    	day = 0;
    	month = 0;
    	year = 0;
    }
    
    public Date(int day,int m_mes,int m_anio)
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public Integer getDia()
    {
       return day;
    }
        
    public Integer getMes()
    {
    	return month;
    }
    
    public Integer getAnio()
    {
    	return year;
    }

    public void setDia(Integer day)
    {
    	this.day=day;
    }
    
    public void setMes(Integer month)
    {
    	this.month=month;
    }
    
    public void setAnio(Integer year)
    {
    	this.year=year;
    }
        
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		
		if (obj == null) return false;
		
		if (getClass() != obj.getClass()) return false;
		
		Date other = (Date) obj;
		
		if (day == null) {
			if (other.day != null) return false;
		}
		else if (!day.equals(other.day)) return false;
		
		if (month == null) {
			if (other.month != null) return false;
		}
		else if (!month.equals(other.month)) return false;
		
		if (year == null) {
			if (other.year != null) return false;
		}
		else if (!year.equals(other.year)) return false;
		
		return true;
	}

	public String toString() {
		return "Fecha:" + day + month + year;
	}
    
}