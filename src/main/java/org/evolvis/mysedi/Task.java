package org.evolvis.mysedi;

public class Task {

    private String title;
    private String description;
    private int duration;

    public Task()
    {
    	title = "";
    	description="";
    	duration = 0;
    }
    
    public String getTitle()
    {
       return title;
    }
    
    public String getDescription()
    {
       return description;
    }
    
    public int getDuration()
    {
       return duration;
    }
    
    public void setTitle(String title)
    {
    	this.title=title;
    }
    
    public void setDescription(String description)
    {
    	this.description=description;
    }
    
    public void setDuration(int duration)
    {
    	this.duration=duration;
    }
    
    @Override
    public String toString()
    {
    	return "Tarea: "+ title + " || " + description + " || " + duration;
    }
}