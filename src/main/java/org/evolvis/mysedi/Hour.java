package org.evolvis.mysedi;

public class Hour
{
    private Integer hour;
    private Integer minutes;

    public Hour()
    {
        hour = 0;
        minutes = 0;
    }
    
    public Hour(int hour, int minutes)
    {
        this.hour = hour;
        this.minutes = minutes;
    }

	public Integer getHour() {
		return hour;
	}

	public void setHour(Integer hour) {
		this.hour = hour;
	}

	public Integer getMinutes() {
		return minutes;
	}

	public void setMinutes(Integer minutes) {
		this.minutes = minutes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hour == null) ? 0 : hour.hashCode());
		result = prime * result + ((minutes == null) ? 0 : minutes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hour other = (Hour) obj;
		if (hour == null) {
			if (other.hour != null)
				return false;
		} else if (!hour.equals(other.hour))
			return false;
		if (minutes == null) {
			if (other.minutes != null)
				return false;
		} else if (!minutes.equals(other.minutes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[" + hour + ":" + minutes + "]";
	}
}