package org.evolvis.mysedi;

import java.util.Comparator;

public class HourComparator implements Comparator<Hour> {

	@Override
	public int compare(Hour o1, Hour o2) {		
		if (o1.getHour().equals(o2.getHour()))
		{
			if (o1.getMinutes().equals(o2.getMinutes())) {
				return 0;
			}
			else if (o1.getMinutes() < o2.getMinutes()) {
				return -1;
			}
			else {
				return 1;
			}
		}
		else if (o1.getHour() < o2.getHour()) {
			return -1;
		}
		else {
			return 1;
		}
	}

}