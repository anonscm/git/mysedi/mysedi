package org.evolvis.mysedi;

public class Complete {
	
	private String id;
	private String hour;
    private String minutes;
    private String day;
    private String month;
    private String year;
    private String title;
    private String description;
    private String duration;

    public Complete(){}
    
    public Complete (String id,String hour,String minutes,String day,String month,String year,String title,
    String description,String duration){
    	
    	this.id=id;
    	this.hour=hour;
    	this.minutes=minutes;
    	this.day=day;
    	this.month=month;
    	this.year=year;
    	this.title=title;
    	this.description=description;
    	this.duration=duration;
    }
    
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
    
	public String getHour() {
		return hour;
	}

	public void setHour(String hora) {
		this.hour = hora;
	}

	public String getMinutes() {
		return minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}
	
	public String getMonth() {
		return month;
	}

	public void setMonth(String mes) {
		this.month = mes;
	}
	
	public String getYear() {
		return year;
	}

	public void setYear(String anio) {
		this.year = anio;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String titulo) {
		this.title = titulo;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String descripcion) {
		this.description = descripcion;
	}
	
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duracion) {
		this.duration = duracion;
	}

	@Override
	public String toString() {
//		return "Completo [day=" + day + ", description=" + description
//				+ ", duration=" + duration + ", hour=" + hour + ", minutes="
//				+ minutes + ", month=" + month + ", title=" + title + ", year="
//				+ year + "]";
		return "\""+getDay()+"/"+getMonth()+"/"+getYear()+" "+getHour()+":"+getMinutes()+
				" || "+getTitle()+" || "+getDescription() + " || " + getDuration();
	}
}